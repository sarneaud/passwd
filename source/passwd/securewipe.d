module passwd.securewipe;

@safe:

import std.traits;

/**
	Guarantee value's memory is wiped with zeroes (even with optimisations enabled)

	It's conservative about the types that are allowed.  If your type isn't allowed, you should possibly consider using a different one for sensitive data.
	For example, it won't erase immutable types like strings (use `char[]` instead) or classes (use a plain-old-data struct instead) or anything containing pointers.
*/
void secureWipe(T)(ref T value) @trusted if (isJustData!T && isMutable!T)
{
	explicit_bzero(&value, T.sizeof);
}

/// ditto
void secureWipe(T)(T[] value) @trusted if (isJustData!T && isMutable!T)
{
	// This should be impossible, but @safe is @safe
	if (value.length > size_t.max / T.sizeof) throw new Error("Array too large to be handled safely");

	explicit_bzero(value.ptr, T.sizeof * value.length);
}

private:

extern(C) void explicit_bzero(void*, size_t) @nogc nothrow;

/**
	True only for types that are raw data value types with no pointers, etc.

	It's meant to test for types that are safe to wipe.
*/
template isJustData(T)
{
	static if (isScalarType!T)
	{
		enum isJustData = true;
	}
	else static if (isStaticArray!T)
	{
		enum isJustData = isJustData!(ForeachType!T);
	}
	else static if (is(T == struct) || is(T == union))
	{
		enum isJustData = __traits(isPOD, T) && !isNested!T && allFieldsJustData!T;
	}
	else
	{
		enum isJustData = false;
	}
}

/**
	Helper for isJustData

	Required because of D's scoping rules.
*/
template allFieldsJustData(S)
{
	import std.meta : allSatisfy;
	enum allFieldsJustData = allSatisfy!(isJustData, Fields!S);
}
