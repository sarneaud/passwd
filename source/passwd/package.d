/**
	Library for UNIX-style password hashing.

	Basic example of password hashing:
	---
	const salt = SHA512Crypt.genSalt();
	// Result looks something like "$6$/CrouvED7qMJ/IbD"
	auto crypted = "hunter2".crypt(salt);
	// Result looks something like "$6$/CrouvED7qMJ/IbD$w2auDz2o61BBLowCbYbO.AIsM5XxSPME3PW2b7P.3qamDP5v4aSwyBPLDKolI/rBjTTGDIhUfUsszNv/DOy0B."
	---
	
	The interface to each algorithm is a struct with static members, as below:
	---
	struct CryptAlgo
	{
		/// Length in bytes of a binary digest
		enum kDigestLength;
		/// Maximum length needed for output of genSalt()
		enum kMaxSaltStrLength;
		/// Maximum length needed for output of crypt()
		enum kMaxCryptStrLength;

		/// Generate a good salt for this algorithm
		static string genSalt();

		/// Generate a good salt for this algorithm and write to an output range
		static void genSalt(Out)(ref Out output) if (isOutputRange!(Out, char));
	}
	---
*/
module passwd;

@safe:

import std.algorithm.comparison : max;
import std.algorithm.searching : findSplit;
import std.digest : secureEqual;
import std.meta : AliasSeq, staticMap;
import std.range;
public import std.typecons : Flag, No, Yes;
import std.utf : byCodeUnit;

import passwd.bcrypt;
import passwd.exception;
import passwd.md5;
import passwd.sha;
import passwd.util;

/**
	Hash `password` according to `salt`

	It's a D version of the standard crypt(3) function
	https://www.freebsd.org/cgi/man.cgi?crypt%283%29

	It's recommended that `salt` be generated using one of the provided `genSalt()` functions.  (E.g., `SHA512Crypt.genSalt()`)

	Overloads are provided that allow writing to a given output range, or using a pre-parsed salt string (see `passwd.util.cryptSplit()`), or optionally only writing the encoded digest (without the salt).  Most users won't need them.

	Note: crypt(3) allows algorithms to sanitise the salt string, so the output isn't guaranteed to be the input salt string concatenated with the encoded digest, unless the salt string was generated correctly by (for example) the provided `genSalt()` functions.
*/
char[] crypt(const(char)[] password, const(char)[] salt)
{
	auto ret_app = appender!(char[]);
	password.crypt(ret_app, salt);
	return ret_app[];
}

/// ditto
void crypt(Out)(const(char)[] password, ref Out output, const(char)[] salt, Flag!"writeSalt" write_salt = Yes.writeSalt) if (isOutputRange!(Out, char))
{
	auto salt_data = cryptSplit(salt);
	password.crypt(output, salt_data, write_salt);
}

///	ditto
void crypt(Out)(const(char)[] password, ref Out output, ref const(CryptPieces) salt_data, Flag!"writeSalt" write_salt = Yes.writeSalt) if (isOutputRange!(Out, char))
{
	switch (salt_data.algo_id)
	{
		case "1":
			MD5Crypt.crypt(password, output, salt_data, write_salt);
			return;
		case "2a":
		case "2b":
			Bcrypt.crypt(password, output, salt_data, write_salt);
			return;
		case "5":
			SHA256Crypt.crypt(password, output, salt_data, write_salt);
			return;
		case "6":
			SHA512Crypt.crypt(password, output, salt_data, write_salt);
			return;
		default:
			throw new NotImplementedException("crypt() algorithm not implemented");
	}
}

///
unittest
{
	const salt = SHA512Crypt.genSalt();
	// Result looks something like "$6$/CrouvED7qMJ/IbD"
	auto crypted = "hunter2".crypt(salt);
	// Result looks something like "$6$/CrouvED7qMJ/IbD$w2auDz2o61BBLowCbYbO.AIsM5XxSPME3PW2b7P.3qamDP5v4aSwyBPLDKolI/rBjTTGDIhUfUsszNv/DOy0B."

	// crypt(3)ed passwords can be erased from memory after use if you are paranoid
	secureWipe(crypted);
	import std.algorithm.searching : all;
	assert (crypted.all!"a == 0");
}

/**
	Test a password against the given crypt(3) string

	This is the recommended way to check an untrusted user's password guess against a password database.  The naïve method of `crypt()`ing the password and comparing it using == is vulnerable to a timing attack that leaks the hashed password, allowing the attacker to run their own guesses offline.

	An attacker timing the response of this function can guess the algorithm used for hashing, but not easily figure out the hash or right password.
*/
bool canCryptTo(const(char)[] password, const(char)[] crypted)
{
	char[kMaxCryptStrLength] buffer;
	auto buffer_p = buffer[].byCodeUnit;
	auto crypted_pieces = cryptSplit(crypted);
	password.crypt(buffer_p, crypted_pieces, No.writeSalt);
	auto digest_txt = buffer[0..$-buffer_p.length];
	return secureEqual(digest_txt.byCodeUnit, crypted_pieces.digest_txt.byCodeUnit);
}

///
unittest
{
	import std.stdio;
	const password_guess = "hunter2";
	const crypted = "$1$ZjzxeLeq$WXTi8xm9qRouh1zB8tyxX0";
	if (password_guess.canCryptTo(crypted))
	{
		// welcomeUserIn();
	}
	else
	{
		// kickUserOut();
		assert (false);
	}
}

private template GetMember(string name)
{
	template GetMember(S)
	{
		enum GetMember = __traits(getMember, S, name);
	}
}
/// Maximum size needed for any genSalt() result
enum kMaxSaltStrLength = max(staticMap!(GetMember!"kMaxSaltStrLength", CryptAlgos));
/// Maximum size needed for any crypt() result
enum kMaxCryptStrLength = max(staticMap!(GetMember!"kMaxCryptStrLength", CryptAlgos));

/// All supported crypt(3) algorithms
alias CryptAlgos = AliasSeq!(MD5Crypt, Bcrypt, SHA256Crypt, SHA512Crypt);
