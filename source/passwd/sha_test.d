module passwd.sha_test;

@safe:

import std.algorithm;
import std.range;
import std.utf : byCodeUnit;

import passwd;
import passwd.exception;
import passwd.sha;
import passwd.test;

unittest
{
	auto salt = SHA256Crypt.genSalt(2000);
	standardTests!SHA256Crypt(salt);
	assertThrown!ValueException("hunter2".crypt("$5$unknownparam=foo$salt"));
}

unittest
{
	auto salt = SHA512Crypt.genSalt(2000);
	standardTests!SHA512Crypt(salt);
	assertThrown!ValueException("hunter2".crypt("$6$unknownparam=foo$salt"));
}

version (unittest)
{
	bool testCase(string password, string salt, string ex_crypt)
	{
		auto result = crypt(password, salt);
		return result == ex_crypt;
	}
}

// SHA256 test vectors
unittest
{
	// https://openwall.info/wiki/john/sample-hashes
	assert ("password".canCryptTo("$5$MnfsQ4iN$ZMTppKN16y/tIsUYs/obHlhdP.Os80yXhTurpBMUbA5"));
	assert ("rasmuslerdorf".canCryptTo("$5$rounds=5000$usesomesillystri$KqJWpanXZHKq2BOB43TSaYhEWsQ1Lr5QNyPCDH/Tp.6"));

	// https://akkadia.org/drepper/SHA-crypt.txt
	assert (testCase("Hello world!", "$5$saltstring", "$5$saltstring$5B8vYYiY.CVt1RlTTf8KbXBH3hsxY/GNooZaBBGWEc5"));
	assert (testCase("Hello world!", "$5$rounds=10000$saltstringsaltstring", "$5$rounds=10000$saltstringsaltst$3xv.VbSHBb41AL9AvLeujZkZRBAwqFMz2.opqey6IcA"));
	assert (testCase("This is just a test", "$5$rounds=5000$toolongsaltstring", "$5$rounds=5000$toolongsaltstrin$Un/5jzAHMgOGZ5.mWJpuVolil07guHPvOW8mGRcvxa5"));
	assert (testCase("a very much longer text to encrypt.  This one even stretches over morethan one line.", "$5$rounds=1400$anotherlongsaltstring", "$5$rounds=1400$anotherlongsalts$Rx.j8H.h8HjEDGomFU8bDkXm3XIUnzyxf12oP84Bnq1"));
	assert (testCase("we have a short salt string but not a short password", "$5$rounds=77777$short", "$5$rounds=77777$short$JiO1O3ZpDAxGJeaDIuqCoEFysAe1mZNJRs3pw0KQRd/"));
	assert (testCase("a short string", "$5$rounds=123456$asaltof16chars..", "$5$rounds=123456$asaltof16chars..$gP3VQ/6X7UUEW3HkBn2w1/Ptq2jxPyzV/cZKmF/wJvD"));
	assert (testCase("the minimum number is still observed", "$5$rounds=10$roundstoolow", "$5$rounds=1000$roundstoolow$yfvwcWrQ8l/K0DAWyuPMDNHpIVlTQebY9l/gL972bIC"));
}

// SHA512 test vectors
unittest
{
	// https://openwall.info/wiki/john/sample-hashes
	assert ("password".canCryptTo("$6$zWwwXKNj$gLAOoZCjcr8p/.VgV/FkGC3NX7BsXys3KHYePfuIGMNjY83dVxugPYlxVg/evpcVEJLT/rSwZcDMlVVf/bhf.1"));
	assert ("rasmuslerdorf".canCryptTo("$6$rounds=5000$usesomesillystri$D4IrlXatmP7rx3P3InaxBeoomnAihCKRVQP22JZ6EY47Wc6BkroIuUUBOov1i.S5KPgErtP/EN5mcO.ChWQW21"));

	// https://akkadia.org/drepper/SHA-crypt.txt
	assert (testCase("Hello world!", "$6$saltstring", "$6$saltstring$svn8UoSVapNtMuq1ukKS4tPQd8iKwSMHWjl/O817G3uBnIFNjnQJuesI68u4OTLiBFdcbYEdFCoEOfaS35inz1"));
	assert (testCase("Hello world!", "$6$rounds=10000$saltstringsaltstring", "$6$rounds=10000$saltstringsaltst$OW1/O6BYHV6BcXZu8QVeXbDWra3Oeqh0sbHbbMCVNSnCM/UrjmM0Dp8vOuZeHBy/YTBmSK6H9qs/y3RnOaw5v."));
	assert (testCase("This is just a test", "$6$rounds=5000$toolongsaltstring", "$6$rounds=5000$toolongsaltstrin$lQ8jolhgVRVhY4b5pZKaysCLi0QBxGoNeKQzQ3glMhwllF7oGDZxUhx1yxdYcz/e1JSbq3y6JMxxl8audkUEm0"));
	assert (testCase("a very much longer text to encrypt.  This one even stretches over morethan one line.", "$6$rounds=1400$anotherlongsaltstring", "$6$rounds=1400$anotherlongsalts$POfYwTEok97VWcjxIiSOjiykti.o/pQs.wPvMxQ6Fm7I6IoYN3CmLs66x9t0oSwbtEW7o7UmJEiDwGqd8p4ur1"));
	assert (testCase("we have a short salt string but not a short password", "$6$rounds=77777$short", "$6$rounds=77777$short$WuQyW2YR.hBNpjjRhpYD/ifIw05xdfeEyQoMxIXbkvr0gge1a1x3yRULJ5CCaUeOxFmtlcGZelFl5CxtgfiAc0"));
	assert (testCase("a short string", "$6$rounds=123456$asaltof16chars..", "$6$rounds=123456$asaltof16chars..$BtCwjqMJGx5hrJhZywWvt0RLE8uZ4oPwcelCjmw2kSYu.Ec6ycULevoBK25fs2xXgMNrCzIMVcgEJAstJeonj1"));
	assert (testCase("the minimum number is still observed", "$6$rounds=10$roundstoolow", "$6$rounds=1000$roundstoolow$kUMsbe306n21p9R.FRkW3IGn.S9NPN0x50YhH1xhLsPuWGsUSklZt58jaTfF4ZEQpyUNGc0dqbpBYYBaHHrsX."));
}
