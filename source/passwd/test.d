module passwd.test;

@safe:

import passwd;
import passwd.exception;

import std.utf : byCodeUnit;

version(unittest)
{
	void standardTests(Algo)(string salt)
	{
		static assert (Algo.kMaxSaltStrLength <= kMaxSaltStrLength);
		static assert (Algo.kMaxCryptStrLength <= kMaxCryptStrLength);

		auto result = "hunter2".crypt(salt);
		assert ("hunter2".canCryptTo(result));
		assert (!"hunter3".canCryptTo(result));
	}
}

unittest
{
	assertThrown!NotImplementedException("hunter2".crypt("$nosuchalgorithm$salt"));
}
