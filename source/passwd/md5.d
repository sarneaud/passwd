/**
	Poul-Henning Kamp's MD5 crypt(3) algorithm

	This algorithm is from the 90s and there's no good reason to use it in new applications.  It's only here because it's a widely supported crypt(3) algorithm that's still not totally insecure.
*/
module passwd.md5;

/*
	This file references the original implementation at from https://svnweb.freebsd.org/base/head/lib/libcrypt/crypt.c?revision=4246&view=markup

	That implementation contains the following notice:

	 * ----------------------------------------------------------------------------
	 * "THE BEER-WARE LICENSE" (Revision 42):
	 * <phk@login.dknet.dk> wrote this file.  As long as you retain this notice you
	 * can do whatever you want with this stuff. If we meet some day, and you think
	 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
	 * ----------------------------------------------------------------------------
*/

@safe:

import std.digest.md;
import std.range.primitives;
public import std.typecons : Flag, No, Yes;

import passwd.util;

/// Routines for MD5-based crypt(3)
struct MD5Crypt
{
	/// Maximum length needed for output of genSalt()
	enum kMaxSaltStrLength = "$1$".length + 8;
	/// Maximum length needed for output of crypt()
	enum kMaxCryptStrLength = kMaxSaltStrLength + 1 + kDigestLength * 8 / 6;

	/// Generate a good salt for this algorithm
	static string genSalt()
	{
		import std.array : appender;
		auto ret_app = appender!string;
		MD5Crypt.genSalt(ret_app);
		return ret_app[];
	}

	/// Generate a good salt for this algorithm and write to an output range
	static void genSalt(Out)(ref Out output) if (isOutputRange!(Out, char))
	{
		foreach (char c; "$1$") output.put(c);
		ubyte[6] random_buf;
		fillSecureRandom(random_buf[]);
		random_buf.cryptB64Encode(output);
	}

	/// Hash password and write full crypt(3) string or just encoded digest to an output range
	static void crypt(Out)(const(char)[] password, ref Out output, ref const(CryptPieces) salt_data, Flag!"writeSalt" write_salt = Yes.writeSalt) if (isOutputRange!(Out, char))
	{
		if (write_salt)
		{
			foreach (char c; "$1$") output.put(c);
			foreach (char c; truncateSalt(salt_data.salt_txt)) output.put(c);
			output.put('$');
		}
		auto digest = digestOf(password, salt_data);
		digest.cryptB64Encode(output);
	}

	private:

	enum kDigestLength = 16;
	alias Digest = ubyte[kDigestLength];

	/**
		Enforce maximum salt string length

		Anything longer is silently ignored.
	*/
	static const(char)[] truncateSalt(const(char)[] salt_txt) pure
	{
		import std.algorithm.comparison : min;
		return salt_txt[0..min(8, $)];
	}

	/// The heavy lifting of hashing the password to a binary digest
	static Digest digestOf(const(char)[] password, ref const(CryptPieces) salt_data)
	{
		import std.string : representation;
		auto salt_bytes = truncateSalt(salt_data.salt_txt).representation;
		auto password_bytes = password.representation;

		// See also Ulrich Drepper's spec for the similar SHA-based crypt

		// Ultimately, this algorithm is a grab bag of things done to mix up the input and not be fast about it.
		// Ours not to reason why. Just reimplement what PHK did.

		MD5 ctx, ctx1;
		scope (exit) { secureWipe(ctx); secureWipe(ctx1); }
		Digest digest;  // "final" in PHK's code, but that's a reserved word in D

		ctx.put(password_bytes);
		ctx.put("$1$".representation);
		ctx.put(salt_bytes);

		// A second hash gets mixed into the first
		ctx1.put(password_bytes);
		ctx1.put(salt_bytes);
		ctx1.put(password_bytes);
		digest = ctx1.finish();
		ctx.stretchPut(digest[], password.length);

		// "Then something really weird..."
		for (size_t i = password.length; i; i >>= 1)
		{
			// It looks like there are a couple of bugs in the original PHK code, but they became part of the standard MD5 crypt hash
			if (i & 1)
			{
				// In the PHK code, this was taken from "final" (digest), but that got zeroed out a few lines earlier
				ctx.put(cast(ubyte)0);
			}
			else
			{
				// In the PHK code, this was actually the jth element, but j was never updated from zero
				ctx.put(password_bytes[0]);
			}
		}

		digest = ctx.finish();

		// Apparently 1000 rounds took 34ms on PHK's 60Mhz Pentium back in 1994
		foreach (i; 0..1000)
		{
			ctx1.start();
			if (i & 1)
			{
				ctx1.put(password_bytes);
			}
			else
			{
				ctx1.put(digest[]);
			}

			if (i % 3) ctx1.put(salt_bytes);
			if (i % 7) ctx1.put(password_bytes);

			if (i & 1)
			{
				ctx1.put(digest[]);
			}
			else
			{
				ctx1.put(password_bytes);
			}

			digest = ctx1.finish();
		}

		// Finally the output gets permuted
		foreach (j; 0..output_swaps.length)
		{
			import std.algorithm.mutation : swap;
			swap(digest[j], digest[output_swaps[j]]);
		}

		return digest;
	}
}

private:

immutable(size_t[]) output_swaps = permSwapDecomposition([
	12, 6, 0,
	13, 7, 1,
	14, 8, 2,
	15, 9, 3,
	5, 10, 4,
	11
]);
