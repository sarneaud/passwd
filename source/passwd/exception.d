module passwd.exception;

@safe:

public import std.exception;

/// Exception for invalid values passed to functions
class ValueException : Exception
{
	mixin basicExceptionCtors;
}

/// Exception for things that just aren't implemented
class NotImplementedException : Exception
{
	mixin basicExceptionCtors;
}
