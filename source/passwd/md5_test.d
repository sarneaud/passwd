module passwd.md5_test;

@safe:

import std.algorithm;
import std.range;
import std.utf : byCodeUnit;

import passwd;
import passwd.exception;
import passwd.md5;
import passwd.test;

unittest
{
	auto salt = MD5Crypt.genSalt();
	standardTests!MD5Crypt(salt);
}

unittest
{
	// https://openwall.info/wiki/john/sample-hashes
	assert ("password".canCryptTo("$1$O3JMY.Tw$AdLnLjQ/5jXF9.MTp3gHv/"));
}
