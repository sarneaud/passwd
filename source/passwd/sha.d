/**
	Ulrich Drepper's SHA-based crypt(3) algorithms, as specified here:
	https://akkadia.org/drepper/SHA-crypt.txt

	The main exports are the `SHA256Crypt` and `SHA512Crypt` structs.  Their `genSalt()` static members take an optional count of rounds.  More rounds means more work in computing the hash — both for friendly applications and password bruteforcers.  The default (as in the spec) is 5000 rounds.
*/
module passwd.sha;

@safe:

import std.algorithm.mutation : swap;
import std.digest.sha;
import std.range.primitives;
import std.string : representation;
import std.traits : ReturnType;
public import std.typecons : Flag, No, Yes;
import std.utf : byCodeUnit;

import passwd.exception;
import passwd.util;

/// Implementation of Ulrich Drepper's SHA256-based crypt algorithm
alias SHA256Crypt = SHACrypt!(SHA256, sha256crypt_output_swaps, "$5$");
/// Implementation of Ulrich Drepper's SHA512-based crypt algorithm
alias SHA512Crypt = SHACrypt!(SHA512, sha512crypt_output_swaps, "$6$");

package:

/// Routines for SHA-based crypt(3) algorithms
struct SHACrypt(Hasher, alias output_swaps, string algo_id)
{
	// Example: $5$rounds=10000$saltstringsaltst$3xv.VbSHBb41AL9AvLeujZkZRBAwqFMz2.opqey6IcA
	/// Maximum length needed for output of genSalt()
	enum kMaxSaltStrLength = "$x$rounds=999999999$".length + kSHACryptSaltBytes * 8 / 6;
	/// Maximum length needed for output of crypt()
	enum kMaxCryptStrLength = kMaxSaltStrLength + 1 + kDigestLength * 8 / 6;

	/// Generate a good salt for this algorithm
	static string genSalt(size_t num_rounds=kDefaultSHACryptRounds)
	{
		import std.array : appender;
		auto ret_app = appender!string;
		ret_app.genSHACryptSalt(algo_id, num_rounds);
		return ret_app[];
	}

	/// Generate a good salt for this algorithm and write to an output range
	static void genSalt(Out)(ref Out output, size_t num_rounds=kDefaultSHACryptRounds) if (isOutputRange!(Out, char))
	{
		output.genSHACryptSalt(algo_id, num_rounds);
	}

	/// Hash password and write full crypt(3) string or just encoded digest to an output range
	static void crypt(Out)(const(char)[] password, ref Out output, ref const(CryptPieces) salt_data, Flag!"writeSalt" write_salt = Yes.writeSalt) if (isOutputRange!(Out, char))
	{
		if (write_salt) salt_data.writeSHACryptSalt(output);
		auto digest = digestOf(password, salt_data);
		digest.cryptB64Encode(output);
	}

	private:

	/// Length in bytes of a binary digest
	enum kDigestLength = ReturnType!(Hasher.finish).length;

	/// Generate only the raw binary output of this hashing algorithm
	static ubyte[kDigestLength] digestOf(const(char)[] password, ref const(CryptPieces) salt_data)
	{
		return shaCryptHash!(Hasher, output_swaps)(password, salt_data);
	}
}

private:

enum kDefaultSHACryptRounds = 5000;
enum kMinSHACryptRounds = 1000;
enum kMaxSHACryptRounds = 999_999_999;
enum kMaxSHACryptSaltValueLength = 16;
enum kSHACryptSaltBytes = 12;

/// Format existing salt data and write to an output range
void writeSHACryptSalt(Out)(ref const(CryptPieces) salt_data, ref Out output) if (isOutputRange!(Out, char))
{
	output.put('$');
	foreach (char c; salt_data.algo_id) output.put(c);
	output.put('$');
	if (!salt_data.params.empty)
	{
		import std.conv : toChars;
		auto num_rounds = getNumRounds(salt_data.params);
		foreach (char c; "rounds=") output.put(c);
		foreach (char c; toChars(num_rounds)) output.put(c);
		output.put('$');
	}
	const(char)[] salt_txt = salt_data.salt_txt;
	if (salt_txt.length > kMaxSHACryptSaltValueLength) salt_txt = salt_txt[0..kMaxSHACryptSaltValueLength];
	foreach (char c; salt_txt) output.put(c);
	output.put('$');
}

/// Generate a new salt string
void genSHACryptSalt(Out)(ref Out output, string algo_id, size_t num_rounds) if (isOutputRange!(Out, char))
{
	foreach (char c; algo_id) output.put(c);

	if (num_rounds != kDefaultSHACryptRounds)
	{
		import std.conv : toChars;
		foreach (char c; "rounds=") output.put(c);
		foreach (char c; toChars(num_rounds)) output.put(c);
		output.put('$');
	}

	ubyte[kSHACryptSaltBytes] random_buf;
	fillSecureRandom(random_buf[]);
	random_buf.cryptB64Encode(output);
}

/**
	Parse the params field from the salt string to get the number of rounds (or return default value)

	According to the spec, the number of rounds silently clips to the min/max values.
*/
size_t getNumRounds(const(char)[] params_str) pure
{
	import std.algorithm.searching : startsWith;
	import std.utf : byCodeUnit;
	size_t num_rounds = kDefaultSHACryptRounds;
	if (params_str.startsWith("rounds="))
	{
		params_str = params_str["rounds=".length..$];
		num_rounds = 0;
		static assert (kMinSHACryptRounds * 10 < size_t.max, "Integer overflow possible");
		while (!params_str.empty && params_str[0] >= '0' && params_str[0] <= '9')
		{
			num_rounds *= 10;
			num_rounds += params_str[0] - '0';
			if (num_rounds > kMaxSHACryptRounds) num_rounds = kMaxSHACryptRounds;
			params_str.popFront();
		}
		if (num_rounds < kMinSHACryptRounds) num_rounds = kMinSHACryptRounds;
	}
	enforce!ValueException(params_str.empty, "Field contains = character but can't be parsed as SHA crypt parameters");
	return num_rounds;
}

///
unittest
{
	assert (getNumRounds("") == kDefaultSHACryptRounds);
	assert (getNumRounds("rounds=4242") == 4242);
	assert (getNumRounds("rounds=0") == kMinSHACryptRounds);
	assert (getNumRounds("rounds=2") == kMinSHACryptRounds);
	assert (getNumRounds("rounds=1000000000000000000000000000000000000") == kMaxSHACryptRounds);
}

/// The heavy lifting of hashing the password to a binary digest
ReturnType!(Hasher.finish) shaCryptHash(Hasher, alias output_swaps)(const(char)[] password, ref const(CryptPieces) salt_data)
{
	alias Digest = ReturnType!(Hasher.finish);
	auto num_rounds = getNumRounds(salt_data.params);
	auto salt_bytes = salt_data.salt_txt.representation;
	auto password_bytes = password.representation;

	if (salt_bytes.length > kMaxSHACryptSaltValueLength) salt_bytes = salt_bytes[0..kMaxSHACryptSaltValueLength];

	// Numbers correspond to steps in Ulrich Drepper's spec
	// 1
	Hasher hasher_out;
	scope (exit) secureWipe(hasher_out);
	// 2
	hasher_out.put(password_bytes);
	// 3
	hasher_out.put(salt_bytes);

	Digest digest_out, digest_b;
	scope (exit) secureWipe(digest_b);
	{
		// 4, 5, 6
		Hasher hasher_b = hasher_out;
		scope (exit) secureWipe(hasher_b);
		// 7
		hasher_b.put(password_bytes);
		// 8
		digest_b = hasher_b.finish();
	}

	// 9, 10
	hasher_out.stretchPut(digest_b, password_bytes.length);

	// 11
	for (size_t j = password_bytes.length; j; j >>= 1)
	{
		if (j & 1)
		{
			// 11a
			hasher_out.put(digest_b[]);
		}
		else
		{
			// 11b
			hasher_out.put(password_bytes);
		}
	}

	// 12
	digest_out = hasher_out.finish();

	Digest digest_dp;
	scope (exit) secureWipe(digest_dp);
	{
		// 13
		Hasher hasher_dp;
		scope (exit) secureWipe(hasher_dp);
		// 14
		foreach (_; 0..password_bytes.length) hasher_dp.put(password_bytes);
		// 1
		digest_dp = hasher_dp.finish();
	}

	Digest digest_ds;
	scope (exit) secureWipe(digest_ds);
	{
		// 17
		Hasher hasher_ds;
		scope (exit) secureWipe(hasher_ds);
		// 18
		foreach (_; 0..16+digest_out[0])
		{
			hasher_ds.put(salt_bytes);
		}
		// 19
		digest_ds = hasher_ds.finish();
	}

	// 21
	Hasher hasher_c;
	scope(exit) secureWipe(hasher_c);
	foreach (j; 0..num_rounds)
	{
		// 21a
		if (j & 1)
		{
			// 21b
			stretchPut(hasher_c, digest_dp, password_bytes.length);
		}
		else
		{
			// 21c
			hasher_c.put(digest_out);
		}

		// 21d
		if (j % 3 != 0) stretchPut(hasher_c, digest_ds, salt_bytes.length);
		// 21e
		if (j % 7 != 0) stretchPut(hasher_c, digest_dp, password_bytes.length);
		if (j & 1)
		{
			// 21f
			hasher_c.put(digest_out);
		}
		else
		{
			// 21g
			stretchPut(hasher_c, digest_dp, password_bytes.length);
		}
		digest_out = hasher_c.finish();
		hasher_c.start();
	}

	// The output gets permuted as a final flourish
	foreach (j; 0..output_swaps.length)
	{
		swap(digest_out[j], digest_out[output_swaps[j]]);
	}

	return digest_out;
}

immutable(size_t[]) sha256crypt_output_swaps = permSwapDecomposition([
	20, 10, 0,
	11, 1, 21,
	2, 22, 12,
	23, 13, 3,
	14, 4, 24,
	5, 25, 15,
	26, 16, 6,
	17, 7, 27,
	8, 28, 18,
	29, 19, 9,
	30, 31
]);

immutable(size_t[]) sha512crypt_output_swaps = permSwapDecomposition([
	42, 21, 0,
	1, 43, 22,
	23, 2, 44,
	45, 24, 3,
	4, 46, 25,
	26, 5, 47,
	48, 27, 6,
	7, 49, 28,
	29, 8, 50,
	51, 30, 9,
	10, 52, 31,
	32, 11, 53,
	54, 33, 12,
	13, 55, 34,
	35, 14, 56,
	57, 36, 15,
	16, 58, 37,
	38, 17, 59,
	60, 39, 18,
	19, 61, 40,
	41, 20, 62,
	63
]);
